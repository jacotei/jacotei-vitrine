<div style="text-align: center">
	<h3>Nenhum produto encontrado!</h3><br>
	<h4>Você será redirecionado em 5 segundos...</h4><br>
</div>

<script>
	setTimeout(
		function() {
			var href = window.location.href;
			var url = href.replace(window.location.search, "");
			window.location.href = url;
		},
	5000);
</script>