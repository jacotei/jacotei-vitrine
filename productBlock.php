<div class="col-sm-2 col-md-2 padding-8">
	<div class="thumbnail thumb">
	  <img class="imagens_produtos" src="<?php echo $imgsrc; ?>" style="height: 90px; width: 90px;" alt="<?php echo $produto['name']; ?>">
	  <div class="caption">
	    <h5 class="text-center descricao_produtos"><?php echo $produto['name']; ?></h5>
	    <p class="preco text-center">R$ <?php echo $oferta['minPrice']; ?></p>
	    <p class="text-center a_vista" style="<?php echo $line_parcelamento; ?>">à vista</p>
	    <p class="text-center parcelamento" style="<?php echo $hide_parcelamento; ?>">ou <?php echo $oferta['installments']; ?> de R$ <?php echo $oferta['monthlyPayment']; ?> sem juros.</p>
	    <p><a href="<?php echo $link; ?>" target="_blank" class="btn entrar-loja center-block" role="button">Entrar na loja</a></p>
	  </div><!-- / Caption-->
	</div><!-- / Thumbnail-->
</div><!-- / col-sm-6-->