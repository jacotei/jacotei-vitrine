# Projeto JáCotei Vitrine #

### Repositorio Git  ###

* mkdir /www
* cd /www
* git clone https://USUARIOBITBUCKET@bitbucket.org/jacotei/jacotei-vitrine.git
* mv jacotei-vitrine vitrine.jacotei.local

### Configurando hosts local ###

* adicionar no arquivo /etc/hosts

```
127.0.0.1 vitrine.jacotei.local
```

### Configurando vhost HTTP ###

Em /etc/apache2/sites-available crie o arquivo vitrine.jacotei.local.conf

```
<VirtualHost *:80>
       ServerName   vitrine.jacotei.local
       ServerAlias  vitrine.jacotei.com.br
       ServerAdmin webmaster@localhost
       
RewriteLog "/var/log/apache2/rewrite.log"
RewriteLogLevel 3

	DocumentRoot /www/vitrine.jacotei.local/
      <Directory />
              Options FollowSymLinks
              AllowOverride None
      </Directory>
      <Directory /www/vitrine.jacotei.local/>
              Options Indexes FollowSymLinks MultiViews
              AllowOverride All
              Order allow,deny
              allow from all
      </Directory>
      ErrorLog /var/log/apache2/error.log
      # Possible values include: debug, info, notice, warn, error, crit,
      # alert, emerg.
      LogLevel warn
      CustomLog /var/log/apache2/access.log combined

</VirtualHost>
```

### Para o apache versão 2.4 ou superior, utilize esta configuração ###


```
<VirtualHost *:80>
	ServerName vitrine.jacotei.local
	ServerAlias vitrine.jacotei.com.br 
	ServerAdmin webmaster@localhost

	DocumentRoot /www/vitrine.jacotei.local/
	<Directory />
		Options FollowSymLinks
		AllowOverride None
	</Directory>
	<Directory /www/vitrine.jacotei.local/>
		Options Indexes FollowSymLinks MultiViews
		AllowOverride All
		Require all granted
	</Directory>

	ErrorLog /var/log/apache2/error-jacotei.log
	# Possible values include: debug, info, notice, warn, error, crit,
	# alert, emerg.
	LogLevel warn
	CustomLog /var/log/apache2/access.jacotei.log combined
</VirtualHost>
```

Instalando o mod rewrite

```
sudo a2enmod rewrite
```

Adicione o site e reinicie o apache

```
sudo a2ensite vitrine.jacotei.local.conf 
sudo service apache2 restart
```

Instalando o Composer

```
curl -sS https://getcomposer.org/installer | php
```

ou caso não tenha o curl

```
php -r "readfile('https://getcomposer.org/installer');" | php
```

Após o download do composer, basta executar:

```
php composer.phar install
```

Caso queira instalar o composer globalmente:

```
mv composer.phar /usr/bin/composer
composer install
```

Agora você poderá digitar composer em qualquer parte do sisema que ele irá funcionar.
