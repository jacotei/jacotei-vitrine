<?php
/**
 * Monta a vitrine
 */

// arquivo de configuracao
$config = parse_ini_file("core/Config.ini.php", true);

// chama o verificador de url
require 'core/Url.php';

// chama o factory para o elasticsearch
require 'core/ElasticFactory.php';

// chama classe de imagens
require 'core/Image.php';

// chama classe para gerar link do track
require 'core/Track2.php';

// instancia objeto
$urlObj			= new Url();
$elasticSearch 	= new ElasticFactory($config['elasticsearch']);
$img 			= new Image($config['img']);
$track2			= new Track2($config['track']);

// define o path do projeto
define('_PATH_', $urlObj->path);

// TODO
// verifica o tamanho da vitrine a ser utilizada
//????????????????????????????????????????????????????


// pesquisa por categoria
$categoriaId = (isset($_GET['catId']) && $_GET['catId'] > 0) ? $_GET['catId'] : $config['search']['default_category'];


if (extension_loaded('newrelic')) {
	newrelic_set_appname('Vitrine jacotei');

	newrelic_custom_metric("Custom/Vitrine/Visualizado/".$urlObj->getParceiro(), 1);

	newrelic_add_custom_parameter('parceiro', $urlObj->getParceiro());
	newrelic_add_custom_parameter('tipo_vitrine', $urlObj->getTipovitrine());
	newrelic_add_custom_parameter('tamanho', $urlObj->getTamanho());
	newrelic_add_custom_parameter('categoria', $categoriaId);
}

// seta os parametros da pesquisa
$params = [
	'index' => $config['elasticsearch']['index'],
    'type' 	=> 'products',
    'from' 	=> 0,
    'size' 	=> $config['search']['qtd_retorno_produto'],
    'body' 	=> '' 
];

$params['body'] = '
{
	"query": {
		"bool": {
			"should": [
				{ "match": { "categoryId": '.$categoriaId.' }}
			],
			"should": [
				{ "match": { "categories.categoryId": '.$categoriaId.' }}
			]
		}
	}
}';

try {
	// faz a pesquisa
	$searchData = $elasticSearch->search($params);
	$resultado = $elasticSearch->createDataReturn($searchData);
	
	if ($resultado['total'] > 0){
		
		// obtenho o valor da divisao do retorno com a qtd de folhas
		$folha_total = floor($config['search']['qtd_retorno_produto'] / $config['vitrine']['qtd_folha']);
		
// 		echo 'qtd_retorno_produto: '.$config['search']['qtd_retorno_produto'].'<br>';
// 		echo 'qtd_folha: '.$config['vitrine']['qtd_folha'].'<br>';
// 		echo 'folha total: '.$folha_total.'<br>';
		
		// contador para o index total de produtos
		$y = 0;
		
		// gera a quantidade de folhas especificadas
		for ($f=0; $f<$config['vitrine']['qtd_folha']; $f++){
			// coloca o total calculado em $folha_total na folha atual
			for ($z=0; $z<$folha_total; $z++){
				$folha[$f][] =  $resultado['produtos'][$y];
				$y++;
			}
		}
		
	}else{
		// se não tem produto dá um reload pra page principal
		// inclusao do html
		include ('topo.php');
		include ('erro.php');
		include ('footer.php');
		die();
	}
	
} catch (Exception $e) {
	
	if(extension_loaded('newrelic')){
		newrelic_custom_metric("Custom/Elasticsearch/Erro", 1);
		newrelic_notice_error($elasticSearch->getInfo(), $e);
	}
	echo 'erro';
	die('AHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH');
}

// inclusao do html
 include 'topo.php';
 include 'conteudo_estrutura.php';
 // GA está inserido no footer
 include 'footer.php';      