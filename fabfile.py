# -*- coding: utf-8 -*-
from fabric.api import task, env, run, local, put, cd
from fabric.contrib.project import rsync_project

env.user = 'apache'
env.key_filename = '~/.ssh/apache-alog-prod.pem'

@task
def production():
    #Lista de serviodores que atendem a aplicaÃ§Ã£o
    env.hosts = ['192.168.100.209','192.168.100.210','192.168.100.211','192.168.100.212']
    

@task
def release_candidate():
    #Servidor de testes
    env.hosts = ['192.168.100.208']

@task
def deploy():
    #deleta projeto antigo
    run('mkdir -p /www/vitrine.jacotei.com.br/')
    #Sync do projeto
    try:
        rsync_project(local_dir='./', remote_dir='/www/vitrine.jacotei.com.br/', exclude=['fabfile.py', '.git', '*.pyc', '.svn', '*/.tag*', 'app/tmp/*'], delete=False, ssh_opts='-o StrictHostKeyChecking=no')
    except:
        pass
