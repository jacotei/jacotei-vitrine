$(document).ready(function(){
	
	$('.navbar li').click(function(){
		$('.navbar li').removeClass('ativo');
		$(this).addClass('ativo');
	});
	
	$('.navbar li a').click(function(event){
		event.preventDefault();
		var href = window.location.href;
		var url = href.replace(window.location.search, "");
		window.location.href = url + '?' + $(this).attr('cat');
	});
	
});