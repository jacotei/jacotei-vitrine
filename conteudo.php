<?php 
	// loop de folhas de produtos
	for ($x=0; $x<$config['vitrine']['qtd_folha']; $x++){
		$class_active = ($x == 0) ? 'active' : '';
		echo '<div class="item '.$class_active.'">';
	
		// contador de quantidade de produtos dentro de uma folha
		$count = 0;
		
		// loop dos produtos dentro da folha
		foreach ($folha[$x] as $key => $produto):
			// pega as ofertas do produto
			$paramsOffer = [
				'index' => $config['elasticsearch']['index'],
				'type' 	=> 'offers',
				'from' 	=> 0,
				'body' 	=> [
					'query' => [
						'bool' => [
							'must' => [
   								[ 'match' => [ 'id' => $produto['id'] ] ],
   								[ 'match' => [ 'minPrice' => $produto['minPrice'] ] ],
							]
						]
					]
				]
			];
			$searchOffer = $elasticSearch->search($paramsOffer);
			$resultadoOffer = $elasticSearch->createDataReturn($searchOffer);

			// se atingir um total de 6 produtos sai da folha atual
			if ($count > 6){
				break;
			}
			
			// se o produto nao tem oferta passa pra proxima iteração
			if ($resultadoOffer['total'] == 0){
				continue;
				
			// se tem oferta
			}else{
				$count++;
				$oferta = $resultadoOffer['produtos'][0];
			}	

			// ajusta quantidade maxima do nome do produto
			$produto['name'] = substr($produto['name'], 0, $config['vitrine']['max_lenght_name']);
			
			// ajusta dados da oferta
			$oferta['minPrice'] = number_format($oferta['minPrice'], 2, ',', '.');
			$oferta['monthlyPayment'] = number_format($oferta['monthlyPayment'], 2, ',', '.');

			// verifica se tem parcelamento
			$hide_parcelamento = ($oferta['installments'] == 0) ? 'display: none' : '';
			$line_parcelamento = ($oferta['installments'] == 0) ? 'margin-bottom: 20px' : '';

			// gera link da imagem em 90x90
			$imgsrc = $img->getImgSource($produto['id'], $img::$IMG_SIZE_PEQ);
			
			// gera link do track para acessar o site do lojista				
			$link = $track2->assembleTrack2($oferta);
								
			// inclui o bloco (view) da oferta
			include 'productBlock.php';
		endforeach;
		echo '</div>';
	}
?>