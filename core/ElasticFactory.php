<?php 
// chama o autoload do vendor (composer)
require 'vendor/autoload.php';

use Elasticsearch\ClientBuilder;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

/**
 * Classe para acessar o elastic search
 * @author marcos
 *
 */
class ElasticFactory
{
	/**
	 * Indica os hosts de acesso
	 * @var Array
	 */
	private $hosts;
	
	/**
	 * Guarda o objeto do cliente de conexão
	 * @var ClientBuilder
	 */
	private $client;
	
	/**
	 * Guarda o objeto do cliente de log (monolog)
	 * @var unknown
	 */
	private $logger;
	
	/**
	 * Construtor
	 * 
	 * @return ElasticFactory
	 */
	public function __construct($config){
		$this->hosts = explode(',', $config['hosts']);
		$this->createClientLog('log/elasticsearch.log');
		$this->createClient();
		
		return $this;
	}
	
	/**
	 * Cria a conexão com o elastic search
	 * @return boolean
	 */
	private function createClient(){
		$singleHandler  = ClientBuilder::singleHandler();
		$multiHandler   = ClientBuilder::multiHandler();
		
		$this->client = ClientBuilder::create()	// instancia o ClientBuilder
			->setHosts($this->hosts)			// informa os hosts de acesso
			->setRetries(1)						// quantidade de tentativas de conexao
			->setLogger($this->logger)     		// informa o cliente de log
			->build();							// cria o objeto cliente
		
		return true;
	}
	
	/**
	 * Cria uma instancia de log
	 * @param string $path
	 */
	private function createClientLog($path){
		$logger = new Logger('elasticSearchLog');
		$logger->pushHandler(new StreamHandler($path, Logger::WARNING));
		
		$this->logger = $logger;
	}

	/**
	 * Retorna o cliente de conexao para uso
	 * @return ClientBuilder
	 */
	public function getClient(){
		return $this->client;
	}
	
	/**
	 * Recebe o termo por ID e retorna os dados
	 * Exemplo:
	 * 
	 * $params = [
     *	'index' => 'my_index',
     *	'type' => 'my_type',
     *	'id' => 'my_id'
	 * ]
	 * 
	 * @param unknown $params
	 */
	public function query($params){
		return $this->client->get($params);
	}
	
	/**
	 * Recebe termos avançados para fazer pesquisa
	 * o conteudo do campo body pode vir em json
	 * Exemplo:
	 * 
	 * $params = [
     *	'index' => 'my_index',
     *	'type' => 'my_type',
     *	'body' => [
     *  	'query' => [
     *      	'match' => [
     *          	'testField' => 'abc'
     *       	]
     *   	]
     *	]
	 *]
	 *
	 * @param string $params
	 */
	public function search($params){
		return $this->client->search($params);
	}
	
	/**
	 * Pega dados da ultima requisição
	 * 
	 */
	public function getInfo(){
		return $this->client->transport->getLastConnection()->getLastRequestInfo();
	}
	
	/**
	 * retorna apenas dados do estatus de conexao da ultima requisição
	 * @return unknown
	 */
	public function getRequestStatus(){
		$info = $this->getInfo();
		
		$ret['status'] = $info['response']['status'];
		$ret['reason'] = $info['response']['reason'];
		$ret['body'] = $info['response']['body'];
		return $ret;
	}
	
	public function createDataReturn($searchData){
		$ret['total'] = $searchData['hits']['total'];
		$ret['pagina'] = 1;
		$ret['totalPorPagina'] = count($searchData['hits']['hits']);
		foreach ($searchData['hits']['hits'] as $produto){
			$ret['produtos'][] = $produto['_source'];
		}
		return $ret;
	}
}