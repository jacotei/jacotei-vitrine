[elasticsearch]
;hosts				= '192.168.0.228:9200' 		; minha maquina local
;hosts				= '54.207.60.104:9200' 		; amazon
hosts				= '192.168.100.111:9200' 	; producao log
cluster				= 'jacotei_search'
index				= 'catalog'

[api]
x_api_key			= '7d8a7868-2728-11e4-ba27-782bcbee4634'
auth_url			= 'http://auth.api.jacotei.com.br'

[search]
default_category 	= 57 						; celulares
qtd_retorno_produto	= 60 						; qtd de produtos a serem retornados
qtd_retorno_oferta	= 1

[img]
cdn_url 		= 'http://img.ijacotei.com.br/'
prefix_product 	= 'produtos/'

[track]
url 		= 'http://track2.jacotei.com.br/'

[vitrine]
max_lenght_name = 60
qtd_folha		= 3