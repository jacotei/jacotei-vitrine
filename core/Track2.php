<?php
Class Track2 {
	
	private $trackUrl;
	private $clickSourceId 	= null;
	private $quiet			= false;
	private $version 		= '1.1';
	private $type 			= 'click';
	// product
	private $product_url;
	private $product_cpc;
	private $product_value;
	private $product_name;
	private $product_type;
	private $product_product_id;
	private $product_offer_id;
	private $product_store_id;
	private $product_store_name;
	private $product_category_id;
	private $product_category_name;
	
	/**
	 * construtor
	 * @param unknown $config
	 */
	public function __construct($config){
		$this->trackUrl = $config['url'];
	}
	
	/**
	 * Seta o id da origem do click
	 * @param unknown $clickSourceId
	 */
	public function setClickSourceId($clickSourceId){
		$this->clickSourceId = $clickSourceId;
	}
	
	/**
	 * calcula o hash
	 * @return string
	 */
	private function calculateHash(){
		$hash['version'] 	= $this->version;
		$hash['type'] 		= $this->type;
		$hash['product'] 	= array(
			'url' 			=> $this->product_url,
			'cpc' 			=> $this->product_cpc,
			'value' 		=> $this->product_value,
			'name' 			=> $this->product_name,
			'type' 			=> $this->product_type,
			'product_id'	=> $this->product_product_id,
			'offer_id' 		=> $this->product_offer_id,
			'store_id' 		=> $this->product_store_id,
			'store_name'	=> $this->product_store_name,
			'category_id'	=> $this->product_category_id,
			'category_name' => $this->product_category_name);
		$hash['crc'] 		= $this->calculateCrc();
		
		$urlEncoded = json_encode($hash);
		$urlEncoded = gzcompress($urlEncoded, 2);
		$urlEncoded = base64_encode($urlEncoded);		
		
		return urlencode($urlEncoded);
	}
	
	/**
	 * Calcula o CRC
	 * @return string
	 */
	private function calculateCrc(){
		
		if (!$this->product_product_id) {
			$this->product_product_id = '';
		}
		
		if (!$this->product_category_id) {
			$this->product_category_id = '';
		}
		
		$checksum = $this->product_type . 
					$this->product_product_id . 
					$this->product_offer_id . 
					$this->product_store_id . 
					$this->product_category_id . 
					number_format($this->product_value, 2, '.', '') . 
					number_format($this->product_cpc, 2, '.', '') . 
					$this->product_url;
		
		return hash('crc32b', $checksum);
		
	}
	
	/**
	 * Gera o link do track
	 * @param unknown $offerData
	 * @return string
	 */
	public function assembleTrack2($offerData){
		$this->product_url 			= $offerData['storeUrl'];
		$this->product_cpc			= (float)$offerData['cpc'];
		$this->product_value		= (float)$offerData['minPrice'];
		$this->product_name			= $offerData['name'];
		$this->product_product_id	= (integer)$offerData['id'];
		$this->product_type			= ($this->product_product_id == null) ? 'unassigned' : 'assigned';
		$this->product_offer_id		= $offerData['offerId'];
		$this->product_store_id		= (integer)$offerData['storeId'];
		$this->product_store_name	= $offerData['storeName'];
		$this->product_category_id	= (integer)$offerData['categoryId'];
		$this->product_category_name= $offerData['categoryName'];
		
		// redireciona com ou sem o quiet
		$redirect = (!$this->quiet) ? 'r' : 'rq';
		
		// cria o url do track
		$url = $this->trackUrl . $redirect . '?h=' . $this->calculateHash();
		
		// coloca o id da origem do click
		if ($this->clickSourceId != null){ 
			$url .= '&c=' . $this->clickSourceId;
		}
		
		return $url;
	}
}