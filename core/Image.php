<?php
Class Image {
	
	private $url;
	private $prefix_product;
	
	public static $IMG_SIZE_PEQ  = "90/90";
	public static $IMG_SIZE_GRD  = "200/200";
	public static $IMG_SIZE_ZOOM = "zoom_site";
	
	/**
	 * construtor
	 * @param unknown $config
	 */
	public function __construct($config){
		$this->url = $config['cdn_url'];
		$this->prefix_product = $config['prefix_product'];		
	}
	
	/**
	 * Gera a sequencia correta do folder
	 * @param unknown $id
	 * @return string
	 */
	private function createFolderSequence($id){
		$id = str_pad($id, 10, "0", STR_PAD_LEFT);
		$group[0] = substr($id, -2);
		$group[1] = substr($id, -4, 2);
		
		return $group;
	}
	
	/**
	 * Gera o caminho da imagem
	 * @param unknown $id
	 * @param unknown $size
	 * @return string
	 */
	public function getImgSource($id, $size){
		$group = $this->createFolderSequence($id);
		$img_url = $this->url.$this->prefix_product.$size.'/'.$group[0].'/'.$group[1].'/'.$id.'.jpg';
		return $img_url;
	}
	
	
	
}