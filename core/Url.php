<?php

Class Url{
	
	public $path;
	private $parceiro;
	private $tipoVitrine;
	private $tamanho;
	
	/**
	 * Construtor
	 */
	public function __construct(){
		
		if (!isset($_GET['parceiro']) || 
			!isset($_GET['tipo_vitrine']) ||
			!isset($_GET['tamanho'])){
			
			$this->erro('Informações insuficientes');
		}
		$this->path = 'http://'.$_SERVER['HTTP_HOST'].'/';
		$this->parceiro = $_GET['parceiro'];
		$this->tipoVitrine = $_GET['tipo_vitrine'];
		$this->tamanho = $_GET['tamanho'];

		// verifica se o parceiro é válido
		if (!$this->checkParceiro()){
			$this->erro('Parceiro inválido');
		}
		
		// verifica se o tipo de vitrine é válida
		if (!$this->checkVitrine()){
			$this->erro('Tipo de vitrine inválida');
		}
		
	}
	
	/**
	 * Mostra msg de erro e encerra
	 * @param unknown $msg
	 */
	private function erro($msg){
		header('Content-Type: text/html; charset=utf-8');
		echo "<h3>{$msg}</h3>";
		die();
	}
	
	/**
	 * getter de parceiro
	 */
	public function getParceiro(){
		return $this->parceiro;
	}
	
	/**
	 * getter de tipo de vitrine
	 */
	public function getTipovitrine(){
		return $this->tipoVitrine;
	}
	
	/**
	 * getter de tamanho
	 */
	public function getTamanho(){
		return $this->tamanho;
	}
	
	/**
	 * Verifica se o parceiro informado é cadastrado
	 * @return boolean
	 */
	private function checkParceiro(){
		include 'ListaParceiros.php';
		
		if (!in_array(strtolower($this->parceiro), $parceiros)){
			return false;
		}
		
		return true;
	}
	
	/**
	 * Verifica se o tipo de vitrine informada é válida
	 * @return boolean
	 */
	private function checkVitrine(){
		include 'ListaTipoVitrine.php';
		
		if (!in_array(strtolower($this->tipoVitrine), $tipoVitrine)){
			return false;
		}
		
		return true;
	}
}