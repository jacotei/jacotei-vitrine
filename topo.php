<!DOCTYPE html>
<html>
<head>
 	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Vitrine JáCotei</title>
	<script src="<?php echo _PATH_;?>js/jquery.min.js"></script>
	<link rel="stylesheet" href="<?php echo _PATH_;?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo _PATH_;?>css/style.css">
</head>
<body>

<div class="conteudo">
	<div class="jumbotron cabecalho">
		<img src="<?php echo _PATH_;?>img/logo-Band.jpg">
		<ul class="navbar nav nav-tabs">
			<li class="ativo"><a href="#" cat="catId=57" data-toggle="tab">Celulares</a></li>
			<li><a href="#" cat="catId=2" data-toggle="tab">Televisores</a></li>
			<li><a href="#" cat="catId=96" data-toggle="tab">Notebooks</a></li>
			<li><a href="#" cat="catId=3" data-toggle="tab">Geladeiras</a></li>
			<li><a href="#" cat="catId=157" data-toggle="tab">Video Games</a></li>
			<li><a href="#" cat="catId=1516" data-toggle="tab">Tablets</a></li>
		</ul>
	</div>
	
	<script>
	$(document).ready(function(){
		<!-- begin -->
	<?php 
		// se veio de outra categoria sem ser a default marca o menu corretamente
		if (isset($_GET['catId'])){
			?>
			$(".navbar li").removeClass("ativo");
			$(".navbar li a[cat*='<?php echo $_GET['catId']; ?>']" ).parent().addClass( "ativo" );
			<?php 
		}
	?>
	});
	</script>